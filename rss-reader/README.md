#Rss view
## Info
The implementation is based on React as view and Redux as state storage.
I have little experience in front-end development, so I did not have time to fully bring the application 
to production ready and fully debug it. I also spent a lot of time fixing browser blocking for cors. 
Therefore, I took a little time for layout.

## Required
For remove block cors, need to open [https://cors-anywhere.herokuapp.com/corsdemo](https://cors-anywhere.herokuapp.com/corsdemo)
and push "Request temporary access to the demo server" button

## Installation
- run `yarn install` or `npm install` in project folder

## Run
In the project directory, you can run:
`yarn start` or `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.
