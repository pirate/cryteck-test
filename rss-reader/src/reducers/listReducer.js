import {CREATE_ITEM, UPDATE_ITEM, SET_ACTIVE_ITEM, SOURCE_ITEM} from "../actions/SourcesActions";

const initialState = {sourceList: [], titleList:[], articleList: [], activeSource: null};

// eslint-disable-next-line import/no-anonymous-default-export
export default function (state = initialState, action) {
    switch (action.type) {
        case CREATE_ITEM: {
            if (state.sourceList.indexOf(action.url) !== -1) {
                return state;
            }
            state.sourceList.push(action.url);
            state.titleList.push({source: action.url, title: action.title});

            return {...state};
        }
        case UPDATE_ITEM: {
            for (let title of state.titleList) {
                if (title.source === action.source) {
                    title.title = action.title
                }
            }
            return {...state};
        }
        case SET_ACTIVE_ITEM: {
            return {...state, activeSource: action.source};
        }
        case SOURCE_ITEM: {
            for (const article of state.articleList) {
                if (article.item.link === action.item.link) {
                    return state;
                }
            }
            state.articleList.push({source: action.source, item: action.item});
            return {...state};
        }

        default:
            return state
    }
}
