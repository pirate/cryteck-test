import FeedMe from 'feedme';
import http from 'http';

export const CREATE_ITEM = 'CREATE_ITEM';
export const UPDATE_ITEM = 'UPDATE_ITEM';
export const SET_ACTIVE_ITEM = 'SET_ACTIVE_ITEM';
export const SOURCE_ITEM = 'SOURCE_ITEM';


export function createItem(url) {
  return (dispatch) => {
    dispatch(createSource(url));
    dispatch(updateArticleList());
  }
}

function createSource(url) {
  return {
    type: CREATE_ITEM,
    url: url
  }
}

export function updateArticleList() {
  return (dispatch, getState) => {
    getState().rss.sourceList.map(getData)
    function getData(url) {
      http.get('https://cors-anywhere.herokuapp.com/' + url, (result) => {
        let parser = new FeedMe(true);
        result.pipe(parser);
        parser.on('title', function (title) {
          dispatch(updateItem(url, title));
        });
        parser.on('item', (item) => {
          dispatch(sourceItem(url, item));
        });
      });
    }
  }
}

export function updateItem(url, title) {
  return {
    type: UPDATE_ITEM,
    title: title,
    source: url
  }
}

export function setActiveItem(source) {
  return {
    type: SET_ACTIVE_ITEM,
    source: source
  }
}

export function sourceItem(url, item) {
  return {
    type: SOURCE_ITEM,
    source: url,
    item: item
  }
}