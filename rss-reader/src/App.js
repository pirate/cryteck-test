import { Col, Row, Container } from 'react-bootstrap';
import { connect } from 'react-redux';
import React, { Component } from 'react';
import FeedView from "./components/FeedView";
import PropTypes from 'prop-types';
import SourceMenu from "./components/SourceMenu";
import {createItem, setActiveItem, updateArticleList} from "./actions/SourcesActions";
import './App.css'

const propTypes = {
  articleList: PropTypes.array,
  activeTitle: PropTypes.string,
  titleList: PropTypes.array,
  activeSource: PropTypes.string,
  sourceList: PropTypes.array,
};

class App extends Component {
  render() {
    return (
        <Container>
          <Row className="show-grid">
            <Col md={4}>
              <div>
                <SourceMenu createItem={url => this.props.createItem(url)}
                            setActiveItem={source => this.props.setActiveItem(source)}
                            updateArticleList={sourceList => this.props.updateArticleList(sourceList)}
                            />
              </div>
            </Col>
            <Col md={8}>
              <FeedView articleList={this.props.articleList} title={this.props.activeTitle}/>
            </Col>
          </Row>
        </Container>
    );
  }
}

function mapStateToProps(state) {
  const articleList = state.rss.articleList.filter(article => article.source === state.rss.activeSource);
  const activeTitle = state.rss.titleList.find(title => title.source === state.rss.activeSource) || {title: ''};

  return {
    articleList,
    activeTitle: activeTitle.title,
    titleList: state.rss.titleList || [],
    activeSource: state.rss.activeSource,
    sourceList: state.rss.sourceList || []
  };
}

function mapDispatchToProps(dispatch) {
  return {
    createItem: url => dispatch(createItem(url)),
    setActiveItem: source => dispatch(setActiveItem(source)),
    updateArticleList: () => dispatch(updateArticleList()),
  }
}

App.propTypes = propTypes;

export default connect(mapStateToProps, mapDispatchToProps)(App);
