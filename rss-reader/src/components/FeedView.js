import React, { Component } from 'react';
import {Card} from 'react-bootstrap';
import PropTypes from 'prop-types';
import './FeedView.scss'

const propTypes = {
  articleList: PropTypes.array,
  title: PropTypes.string
};

class FeedView extends Component {

  render() {
    const articleList = this.props.articleList.map(article => (
    <Card key={article.item.link}>
      <Card.Body>
        <Card.Title>{article.item.title}</Card.Title>
        <Card.Text>
          {article.item.description}
        </Card.Text>
        <Card.Link target="_blank" href={article.item.link}>Open</Card.Link>
      </Card.Body>
    </Card>
    ))


    return (
      <div className="feed-view">
        <h2 className="feed-title">
          {this.props.title || "Rss view"}
        </h2>
        <h3>Feed</h3>
        {articleList}
      </div>
    );
  }
}

FeedView.propTypes = propTypes;

export default FeedView;

