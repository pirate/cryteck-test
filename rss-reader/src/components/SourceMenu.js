import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Button, FormControl, InputGroup, ListGroup, Modal} from "react-bootstrap";
import './SourceMenu.scss'
import {connect} from "react-redux";

const propTypes = {
  titleList: PropTypes.array.isRequired,
  createItem: PropTypes.func.isRequired,
  setActiveItem: PropTypes.func.isRequired,
  activeSource: PropTypes.string,
  sourceList: PropTypes.array,
};

class SourceMenu extends Component {

  constructor(props) {
    super(props);

    this.state = {
      show: false,
      url: ''
    };
    this.handleClose = this.handleClose.bind(this);
    this.handleShow = this.handleShow.bind(this);
    this.updateUrl = this.updateUrl.bind(this);
  }

  renderSource() {
      return this.props.titleList.map((title) => {
        return (
          <ListGroup.Item key={title.source} action onClick={() => this.props.setActiveItem(title.source)}>
            {title.title}
          </ListGroup.Item>
        )
      })
  }

  handleClose = () => {
    this.setState({ show: false })
  };
  handleShow = () => {
    this.setState({ show: true })
  };
  updateUrl = (event) => {
    this.setState({ url: event.target.value })
  };
  createSource = () => {
    this.props.createItem(this.state.url);
    this.setState({ show: false })
  };

  renderModal() {
    return (
      <>
        <Button variant="primary" onClick={this.handleShow}>
          ADD
        </Button>
        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>New Feed</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <label htmlFor="source-url">URL</label>
            <InputGroup className="mb-3">
              <InputGroup.Prepend>
                <InputGroup.Text id="source-input">
                </InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl id="source-url" aria-describedby="source-input" onChange={this.updateUrl}/>
            </InputGroup>

          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              Cancel
            </Button>
            <Button variant="primary" onClick={this.createSource}>
              OK
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }

  render() {
    return (
      <div className="source-menu">
        <h2>Menu</h2>
        <ListGroup className="source-list" >
          {this.renderSource()}
        </ListGroup>
        <div className="button-group">
            {this.renderModal()}
            <Button variant="secondary" onClick={() => this.props.updateArticleList()}>
              Refresh
            </Button>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    titleList: state.rss.titleList || [],
    activeSource: state.rss.activeSource,
    sourceList: state.rss.sourceList || []
  };
}

SourceMenu.propTypes = propTypes;

export default connect(mapStateToProps)(SourceMenu);

