import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import configureStore from "./redux/configureStore";
import { Provider } from 'react-redux';
import {Route} from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { Router } from 'react-router'

/*
ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
*/

const history = createBrowserHistory();
const store = configureStore({}, history);
const component = (
  <Provider store={store}>
    <Router history={history}>
      <Route path="/" component={App}/>
    </Router>
  </Provider>
);
ReactDOM.render(component, document.getElementById('root'));
