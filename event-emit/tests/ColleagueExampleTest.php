<?php

namespace savelev\emitter\tests;

use PHPUnit\Framework\TestCase;
use savelev\emitter\Colleague;
use savelev\emitter\EventEmitterInterface;

/**
 * Class ColleagueExampleTest
 * @package savelev\emitter\tests
 */
class ColleagueExampleTest extends TestCase
{
    private const EVENT_FOR_TEST = "test_colleague";

    public function testSubscribeOnEvent()
    {
        $eventEmitter = $this->getMockBuilder(EventEmitterInterface::class)->getMock();

        $eventEmitter->expects($this->once())->method('on');
        $colleague = $this->getMockForAbstractClass(Colleague::class, [$eventEmitter]);
        $colleague->subscribeOnEvent(self::EVENT_FOR_TEST, $this->getListener());
    }

    public function testUnSubscribeOnEvent()
    {
        $eventEmitter = $this->getMockBuilder(EventEmitterInterface::class)->getMock();

        $eventEmitter->expects($this->once())->method('removeListener');
        $colleague = $this->getMockForAbstractClass(Colleague::class, [$eventEmitter]);
        $colleague->unSubscribeFromEvent(self::EVENT_FOR_TEST, $this->getListener());
    }

    public function testSendEvent()
    {
        $eventEmitter = $this->getMockBuilder(EventEmitterInterface::class)->getMock();

        $eventEmitter->expects($this->once())->method('emit');
        $colleague = $this->getMockForAbstractClass(Colleague::class, [$eventEmitter]);
        $colleague->sendEvent(self::EVENT_FOR_TEST, 1, 2);
    }

    /**
     * @return callable
     */
    private function getListener(): callable
    {
        return function (array $arguments) {
            return count($arguments);
        };
    }
}
