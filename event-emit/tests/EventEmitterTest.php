<?php

namespace savelev\emitter\tests;

use PHPUnit\Framework\TestCase;
use savelev\emitter\EventEmitter;

/**
 * Class EventEmitterTest
 * @package savelev\emitter\tests
 */
class EventEmitterTest extends TestCase
{
    private const EVENT_FOR_TEST = "test";
    private const SECOND_EVENT_FOR_TEST = "test2";

    public function testOn(): void
    {
        $mock = $this->createPartialMock(\stdClass::class, ['__invoke']);

        $eventEmitter = $this->getEmitter([$mock, '__invoke']);

        $mock->expects(self::once())
            ->method('__invoke');

        $eventEmitter->emit(self::EVENT_FOR_TEST, 1, 2);
    }

    public function testRemoveListener(): void
    {
        $mock = $this->createPartialMock(\stdClass::class, ['__invoke']);

        $eventEmitter = $this->getEmitter([$mock, '__invoke']);

        $eventEmitter->removeListener(self::EVENT_FOR_TEST, [$mock, '__invoke']);

        $mock->expects($this->never())
            ->method('__invoke');

        $eventEmitter->emit(self::EVENT_FOR_TEST, 1, 2);
    }

    public function testRemoveAllListenersByEvent(): void
    {
        $mock = $this->createPartialMock(\stdClass::class, ['__invoke']);
        $secondMock = $this->createPartialMock(\stdClass::class, ['__invoke']);
        $thirdMock = $this->createPartialMock(\stdClass::class, ['__invoke']);

        $eventEmitter = $this->getEmitter([$mock, '__invoke']);

        $eventEmitter->on(self::EVENT_FOR_TEST, [$secondMock, '__invoke']);
        $eventEmitter->on(self::SECOND_EVENT_FOR_TEST, [$thirdMock, '__invoke']);

        $eventEmitter->removeAllListeners(self::EVENT_FOR_TEST);

        $mock->expects($this->never())
            ->method('__invoke');

        $secondMock->expects($this->never())
            ->method('__invoke');

        $thirdMock->expects(self::once())
            ->method('__invoke');

        $eventEmitter->emit(self::EVENT_FOR_TEST, 1, 2);
        $eventEmitter->emit(self::SECOND_EVENT_FOR_TEST, 1, 2);
    }

    public function testRemoveAllListenersForAllEvents(): void
    {
        $mock = $this->createPartialMock(\stdClass::class, ['__invoke']);

        $eventEmitter = $this->getEmitter([$mock, '__invoke']);

        $otherMock = $this->createPartialMock(\stdClass::class, ['__invoke']);

        $eventEmitter->on(self::SECOND_EVENT_FOR_TEST, [$otherMock, '__invoke']);

        $eventEmitter->removeAllListeners();

        $mock->expects($this->never())
            ->method('__invoke');

        $otherMock->expects($this->never())
            ->method('__invoke');

        $eventEmitter->emit(self::EVENT_FOR_TEST, 1, 2);
        $eventEmitter->emit(self::SECOND_EVENT_FOR_TEST, 1, 2);
    }

    public function testEmitWithTwoListeners(): void
    {
        $mock = $this->createPartialMock(\stdClass::class, ['__invoke']);
        $secondMock = $this->createPartialMock(\stdClass::class, ['__invoke']);

        $eventEmitter = $this->getEmitter([$mock, '__invoke']);

        $eventEmitter->on(self::EVENT_FOR_TEST, [$secondMock, '__invoke']);

        $mock->expects(self::once())
            ->method('__invoke');

        $secondMock->expects(self::once())
            ->method('__invoke');

        $eventEmitter->emit(self::EVENT_FOR_TEST, 1, 2);
    }

    /**
     * @param callable $listener
     * @return EventEmitter
     */
    private function getEmitter(callable $listener): EventEmitter
    {
        $eventEmitter = new EventEmitter();

        $eventEmitter->on(self::EVENT_FOR_TEST, $listener);

        return $eventEmitter;
    }
}
