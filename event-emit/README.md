# PHP task Event emitter
In my opinion, the proposed interface already contains the basics of the PubSub pattern. I think, given this interface,
to implement the mediator pattern, the best solution is to keep the message sending logic in the EventEmitter.

## Installation

Run in project folder
```
composer install
```

## Run test

Just run:
```
vendor/bin/phpunit
```

