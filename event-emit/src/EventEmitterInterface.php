<?php


namespace savelev\emitter;

/**
 * Interface EventEmitterInterface
 * @package savelev\emitter
 */
interface EventEmitterInterface
{
    public function on(string $event, callable $listener);

    public function removeListener(string $event, callable $listener);

    public function removeAllListeners(string $event = null);

    public function emit(string $event, ...$arguments);
}
