<?php


namespace savelev\emitter;

/**
 * Class Colleague
 * @package savelev\emitter
 */
abstract class Colleague
{
    protected EventEmitterInterface $eventEmitter;

    /**
     * Colleague constructor.
     * @param EventEmitterInterface $eventEmitter
     */
    public function __construct(EventEmitterInterface $eventEmitter)
    {
        $this->eventEmitter = $eventEmitter;
    }

    /**
     * @param string $event
     * @param callable $listener
     */
    public function subscribeOnEvent(string $event, callable $listener): void
    {
        $this->eventEmitter->on($event, $listener);
    }

    /**
     * @param string $event
     * @param callable $listener
     */
    public function unSubscribeFromEvent(string $event, callable $listener): void
    {
        $this->eventEmitter->removeListener($event, $listener);
    }

    /**
     * @param string $event
     * @param mixed ...$arguments
     */
    function sendEvent(string $event, ...$arguments): void
    {
        $this->eventEmitter->emit($event, ...$arguments);
    }
}
