<?php


namespace savelev\emitter;

/**
 * Class EventEmitter
 * @package savelev\emitter
 */
class EventEmitter implements EventEmitterInterface
{
    private array $channelList;

    /**
     * @param string $event
     * @param callable $listener
     */
    public function on(string $event, callable $listener): void
    {
        $this->channelList[$event][] = $listener;
    }

    /**
     * @param string $event
     * @param callable $listener
     */
    public function removeListener(string $event, callable $listener): void
    {
        if (empty($this->channelList[$event])) {
            return;
        }
        if (($key = array_search($listener, $this->channelList[$event])) !== false) {
            unset($this->channelList[$event][$key]);
        }
    }

    /**
     * @param string|null $event
     */
    public function removeAllListeners(string $event = null): void
    {
        if ($event === null) {
            $this->channelList = [];
            return;
        }
        if (empty($this->channelList[$event])) {
            return;
        }
        unset($this->channelList[$event]);
    }

    /**
     * @param string $event
     * @param mixed ...$arguments
     */
    public function emit(string $event, ...$arguments): void
    {
        if (empty($this->channelList[$event])) {
            return;
        }
        foreach ($this->channelList[$event] as $listener) {
            $listener($arguments);
        }
    }
}
