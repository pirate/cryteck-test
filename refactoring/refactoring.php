<?php

class Order
{
    public const STATUS_PAID = 10;
    public const STATUS_IN_PROCESS = 20;

    public int $id;
    public string $name;
    //Need to migrate data from float
    public int $price;
    //If data migration is possible, it is better to use a numeric field. Search will be faster on it
    public int $status;
}

class OrderService
{
    private Order $order;
    private ?string $address = null;

    function setOrder(Order $order): void
    {
        $this->order = $order;
    }

    private function getOrder(): Order
    {
        return $this->order;
    }

    public function process(): void
    {
        if ($this->order->status === Order::STATUS_PAID) {
            $address = $this->getAddress();
            $shipment = new Shipment();
            $shipment->setOrder($this->order);
            $shipment->setAddress($address);
            //I would also suggest implementing a repository to interact with the data store.
            // And save the model inside the repository.
            $shipment->save();
            $this->order->status = Order::STATUS_IN_PROCESS;
            $this->order->save();
        }
    }

    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }
}
