#!/bin/bash

function ShowHelp {
    echo 'Usage:'
    echo '    ll.sh [DIR] [options]'
    echo ''
    echo 'Commands:'
    echo '  --filter|-f                              Display all the files of a directory tree matching a given regexp'
    echo '  --help|-h                                This help'
}

function Filter {
  if ! [[ -d $DIR ]]; then
    echo "$DIR does not exist"
      exit 1
  fi
  PATTERN="grep -e $2"
  find $DIR -type f | grep -e $FILTER

}

DIR=$1

for i in "$@"
do
  case $i in
    --filter=*|-f=*)
      FILTER="${i#*=}"
      Filter
    shift
    ;;
    --help|-h)
      ShowHelp
    ;;
  esac
  shift
done

